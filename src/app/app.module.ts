import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule ,HttpClientJsonpModule} from '@angular/common/http';
import {JsonpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { ChartComponent } from './layout/chart/chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner'
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,

  ],
  imports: [

    ToastrModule.forRoot(),
    NgxChartsModule,
    HttpClientModule,
    JsonpModule,
    Ng4GeoautocompleteModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientJsonpModule,
    Ng2LoadingSpinnerModule.forRoot({
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff',
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

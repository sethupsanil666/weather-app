import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Jsonp } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private appId = '55a85a28fca3d09ca48a766f6d584644';
  private url = 'https://api.openweathermap.org/data/2.5/';

  constructor(private http: HttpClient) { }
  getForcast(lat, lan, unit = 'metric') {
    return this.http.get(`${this.url}forecast?lat=${lat}&lon=${lan}&appid=${this.appId}&units=${unit}`);

  }

  handleError(text, err = null): any {
    console.log(text, err);
  }

  public getWeather(coordinates: any) {

    return this.http.jsonp('https://weather.cit.api.here.com/weather/1.0/report.json?product=forecast_7days_simple&latitude='
      + coordinates.latitude + '&longitude=' + coordinates.longitude
      + '&app_id=QO4pWfj9mFShoHwBh4Cw&app_code=IpuysMjRspcWVL4DRcoshw', 'jsonpCallback')
      .pipe(map(result => (result as any).dailyForecasts.forecastLocation))

  }

}
export interface WeeklyWeatherObject {
  forecast: Forecast[];
  country: string;
  state: string;
  city: string;
  latitude: number;
  longitude: number;
  distance: number;
  timezone: number;
}

export interface Forecast {
  daylight: string;
  description: string;
  skyInfo: string;
  skyDescription: string;
  temperatureDesc: string;
  comfort: string;
  highTemperature: string;
  lowTemperature: string;
  humidity: string;
  dewPoint: string;
  precipitationProbability: string;
  precipitationDesc: string;
  rainFall: string;
  snowFall: string;
  airInfo: string;
  airDescription: string;
  windSpeed: string;
  windDirection: string;
  windDesc: string;
  windDescShort: string;
  beaufortScale: string;
  beaufortDescription: string;
  uvIndex: string;
  uvDesc: string;
  barometerPressure: string;
  icon: string;
  iconName: string;
  iconLink: string;
  dayOfWeek: string;
  weekday: string;
  utcTime: string;
}


interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}


interface City {
  id: number;
  name: string;
  coord: Coord;
  country: string;
  population: number;
  timezone: number;
}

interface Coord {
  lon: number;
  lat: number;
}
export interface ForcastObject {
  cod: string;
  message: number;
  cnt: number;
  list: List[];
  city: City;
}

interface City {
  id: number;
  name: string;
  coord: Coord;
  country: string;
  population: number;
  timezone: number;
  sunrise: number;
  sunset: number;
}

interface Coord {
  lat: number;
  lon: number;
}

interface List {
  dt: number;
  main: Main;
  weather: Weather[];
  clouds: Clouds;
  wind: Wind;
  sys: Sys;
  dt_txt: string;
  rain?: Rain;
}

interface Rain {
  '3h': number;
}

interface Sys {
  pod: string;
}

interface Wind {
  speed: number;
  deg: number;
}

interface Clouds {
  all: number;
}

interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

interface Main {
  temp: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  sea_level: number;
  grnd_level: number;
  humidity: number;
  temp_kf: number;
}

import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor() { }

  public utcToIstTime(time) {
    try {
      return moment.utc(time).local().format(' hh:mm A');
    } catch (error) {
      return time
    }
  }
  public utcToIstDate(time) {
    try {
      return moment.utc(time).local().format('dddd');
    } catch (error) {
      return time
    }
  }
  public secondsToDate(time) {
    try {
      return moment.unix(time).format('dd D MMM');
    } catch (error) {
      return '-';
    }
  }
}

import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnChanges {
  @Input() single = [{
    name: 'Temprature',
    series: [],
  }];
  public view: any[] = [700, 400];
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = false;
  public showXAxisLabel = true;
  public xAxisLabel: 'Celcious';
  public showYAxisLabel = true;
  public yAxisLabel: 'Temprature';
  public graphDataChart: any[];
  public colorScheme = {
    domain: ['#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor() {
  }
  ngOnInit() {
  }
  ngOnChanges(value: SimpleChanges) {
    console.log(value)
  }
}

import { FunctionsService } from './service/functions.service';
import { WeatherService, ForcastObject, WeeklyWeatherObject, Forecast } from './service/weather.service';
import { Component, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [WeatherService]
})
export class AppComponent implements OnInit {
  showChart = false;
  showTable = false;
  showChartLoader = false;
  showTableLoader = false;
  retry: number;
  lat: any;
  lan: any;
  tableData: Forecast[] = [];
  data = [{
    name: 'Temprature',
    series: [],
  }];
  subs = new SubSink();
  public view: any[] = [700, 400];
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = false;
  public showXAxisLabel = true;
  public xAxisLabel: 'Celcious';
  public showYAxisLabel = true;
  public yAxisLabel: 'Temprature';
  public graphDataChart: any[];
  public colorScheme = {
    domain: ['#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor(private api: WeatherService, private comon: FunctionsService,
              private toastr: ToastrService) { }
  ngOnInit() {
    this.setCurrentLocation();
    setInterval(() => {
      if (this.lat && this.lan) {
        this.getForcastedWeather(this.lat, this.lan);
        this.getNextWeekData(this.lat, this.lan);
      }
    }, 180000);

  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lan = position.coords.longitude;
        this.getForcastedWeather(this.lat, this.lan);
        this.getNextWeekData(this.lat, this.lan);
      });
    }
  }
  autoCompleteCallback1(selectedData: GooglePlaceObject) {
    try {
      this.lat = selectedData.data.geometry.location.lat;
      this.lan = selectedData.data.geometry.location.lng;
      console.log(selectedData);
      this.getForcastedWeather(this.lat, this.lan);
      this.getNextWeekData(this.lat, this.lan);
    } catch (error) {

    }
  }
  utcToDate(time) {
    return this.comon.utcToIstDate(time);
  }
  secondsToTime(time) {
    return this.comon.secondsToDate(time);
  }
  getForcastedWeather(lat, lan) {
    this.showChartLoader = true;
    this.showChart = false;
    this.data[0].series = [];
    this.subs.add(
      this.api.getForcast(lat, lan).subscribe((res: ForcastObject) => {
        if (res.cod == '200') {
          this.showChart = true;
          this.showChartLoader = false;
          let count = 0;
          this.retry = 0;
          res.list.forEach(element => {
            if (count != 10) {
              this.data[0].series.push({
                name: this.comon.utcToIstTime(element.dt_txt),
                value: element.main.temp
              });
              count++;
            }
          });

        }
      }, error => {
        this.error(lat, lan);
      })
    );
  }
  getNextWeekData(lat, lan) {
    try {
      this.showTableLoader = true;
      const data = {
        longitude: lan,
        latitude: lat
      };
      this.subs.add(
        this.api.getWeather(data).subscribe((res: WeeklyWeatherObject) => {
          try {
            this.retry = 0;

            this.showTable = true;
            this.showTableLoader = false;
            this.tableData = res.forecast;
          } catch (err) { }

        }, error => {
          this.error(lat, lan);
        })
      );
    } catch (error) {
    }
  }
  error(lat, lan) {
    this.showChartLoader = false;
    this.showTableLoader = false;
    if (this.retry > 4) {
      this.getNextWeekData(lat, lan);
      this.getForcastedWeather(lat, lan);
      this.retry++;
    } else {
      this.retry = 0;
      this.lan = null;
      this.lan = null;
      this.toastr.warning('Check your network connection');
    }

  }
  showLabel(index) {
    if (index == 0) {
      return 'bg-light';
    }
    return '';
  }
}

interface GooglePlaceObject {
  response: boolean;
  data: Data;
}

interface Data {
  address_components: Addresscomponent[];
  adr_address: string;
  formatted_address: string;
  geometry: Geometry;
  icon: string;
  id: string;
  name: string;
  photos: Photo[];
  place_id: string;
  reference: string;
  scope: string;
  types: string[];
  url: string;
  utc_offset: number;
  vicinity: string;
  website: string;
  html_attributions: any[];
  utc_offset_minutes: number;
  description: string;
  active: boolean;
}

interface Photo {
  height: number;
  html_attributions: string[];
  width: number;
}

interface Geometry {
  location: Location;
  viewport: Viewport;
}

interface Viewport {
  south: number;
  west: number;
  north: number;
  east: number;
}

interface Location {
  lat: number;
  lng: number;
}

interface Addresscomponent {
  long_name: string;
  short_name: string;
  types: string[];
}
